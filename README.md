# kpatch-build

## 介绍
kpatch-build 是一款热补丁制作工具，可在不重启系统和中断业务的情况下对操作系统内核进行CVE和Bug修复。

支持架构：
X86，ARM64

上游社区：https://github.com/dynup/kpatch

#### 软件架构
软件架构说明
![软件架构说明](image.png)

## 安装教程

教程以龙蜥操作系统Anolis OS 8.6，内核版本4.19.91-26.4.an8.x86_64为例说明热补丁制作全流程。

### 1.安装软件包
```
yum install -y make gcc patch bison flex openssl-devel elfutils elfutils-devel dwarves bc perl
```
在制作热补丁过程中出现命令或头文件找不到，可以根据出错提示安装对应软件包即可。

### 2.下载kernel-debuginfo,kernel-devel,kernel source

源代码下载地址： [https://anas.openanolis.cn/errata/detail/ANSA-2022:0692](https://anas.openanolis.cn/errata/detail/ANSA-2022:0692)
debuginfo地址：[https://mirrors.aliyun.com/anolis/8.6/Plus/x86_64/debug/Packages/](https://mirrors.aliyun.com/anolis/8.6/Plus/x86_64/debug/Packages/)
kernel-devel: [https://anas.openanolis.cn/errata/detail/ANSA-2022:0692](https://anas.openanolis.cn/errata/detail/ANSA-2022:0692)

分别解压三个软件包(rpm2cpio xxx.rpm | cpio -div)，依次提取出vmlinux, .config和kernel source，并放置在同一个目录。


### 3.下载kpatch-build
```
wget https://gitee.com/anolis/kpatch-build/repository/archive/master.zip
```

### 4.编译kpatch-build

解压master.zip,并编译安装kpatch-build
```
make BUILDMOD=no && make install
```

### 5.制作热补丁
```
kpatch-build -n kpatch-test  -s /root/hotfix/linux-4.19.91-26.4.an7 -c /root/hotfix/.config -v /root/hotfix/vmlinux -o /root/hotfix/output/ -dddddd -R  /root/hotfix/test-livepatch.patch 
```
其中：
```
-n 补丁名称
-s：指向源代码目录
-c: config文件
-v: vmlinux文件
-o: 产物输出目录
test-livepatch.patch：补丁文件
-d: 输出debug信息
```
## 使用说明
热补丁中自带kpatch管理工具
加载热补丁：
```
kpatch load kpatch-test.ko
```
卸载热补丁：
```
kpatch unload kpatch-test.ko
```
热补丁列表：
```
kpatch list 
```
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

限制
------
- 注意：使用kpatch-build存在一定的限制，但是其中也有一些规避的方法。详情可以查看[用户指导](doc/patch-author-guide.md)
- 如果你的patch里面是修改了__init函数(由`__init`修饰的函数)的话，这种修改目前kpatch-build也是不支持的。
- 如果你的补丁针对已经静态分配内存的数据的修改，kpatch-build不是直接支持的。当然，kpatch-build中提供了一种shadow变量的方式来进行弥补，详情可见[用户指导](doc/patch-author-guide.md)
- 那些修改了函数与动态分配数据的交互方式的补丁不一定是安全的。kpatch-build不会去验证这一类补丁的安全性。这个完全取决于patch的制作者是不是对其修改有足够的认识，以及对一个正在运行的系统打上这样的补丁会对系统造成什么潜在的影响。
- 修改vdso函数是不支持的，因为这些函数运行在用户态下，并且ftrace hook不了它们。
- 修改那些没有fentry的函数也是不支持的。kpatch-build基于ftrace进行函数跟踪，没有ftrace点将使kpatch无法工作。包括那些为了后续链接的所有链接到`lib.a`的`lib-y`的目标（打个比方：`lib/string.o`）